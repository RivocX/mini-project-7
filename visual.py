import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('search_results.csv')

plt.figure(figsize=(10, 6))
bars = plt.bar(df['School Name'], df['Score'], color='skyblue', width=0.5)
for bar, score in zip(bars, df['Score']):
    plt.text(bar.get_x() + bar.get_width() / 2, score, f'{score:.4f}', ha='center', va='bottom')

plt.xlabel('School Name', labelpad=20)
plt.ylabel('Score', labelpad=20)

plt.title('Query Results Visualization')
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.15)

plt.show()
