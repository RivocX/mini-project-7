use anyhow::{Result};
use qdrant_client::prelude::*;
use qdrant_client::qdrant::{
    CreateCollection, PointStruct, SearchPoints, Condition, Filter, VectorParams, VectorsConfig
};
use qdrant_client::qdrant::vectors_config::Config;
use serde_json::json;
use std::convert::TryInto;
use csv::Writer;

#[tokio::main]
async fn main() -> Result<()> {
    let client = initialize_qdrant_client().await?;

    let collection_name = "school_collection";
    create_school_collection(&client, collection_name).await?;
    ingest_school_vectors(&client, collection_name).await?;
    query_school_vectors(&client, collection_name).await?;
    query_school_vectors_with_filter(&client, collection_name).await?;

    Ok(())
}

// Initializing the Qdrant Client
async fn initialize_qdrant_client() -> Result<QdrantClient> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;
    Ok(client)
}

// Create a Qdrant data collection that is used to store school data
async fn create_school_collection(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let _ = client.delete_collection(collection_name).await;

    client.create_collection(&CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Cosine.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    Ok(())
}

// Importing school data into a Qdrant collection
async fn ingest_school_vectors(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let points = vec![
        PointStruct::new(
            1,
            vec![0.85, 0.75, 0.65, 0.95],
            json!({"school_name": "Greenwood High", "location": "Berlin"}).try_into().unwrap(),
        ),
        PointStruct::new(
            2,
            vec![0.88, 0.78, 0.60, 0.90],
            json!({"school_name": "Maple Leaf Academy", "location": "London"}).try_into().unwrap(),
        ),
        PointStruct::new(
            3,
            vec![0.80, 0.85, 0.70, 0.88],
            json!({"school_name": "Riverdale High", "location": "Paris"}).try_into().unwrap(),
        ),
        PointStruct::new(
            4,
            vec![0.89, 0.86, 0.72, 0.85],
            json!({"school_name": "Université PSL", "location": "Paris"}).try_into().unwrap(),
        ),
        PointStruct::new(
            5,
            vec![0.84, 0.85, 0.76, 0.87],
            json!({"school_name": "Duke University", "location": "Durham"}).try_into().unwrap(),
        ),
        // Add more schools as needed...
    ];

    client.upsert_points_blocking(collection_name, None, points, None).await.unwrap();

    Ok(())
}

// Perform query operations on school data
async fn query_school_vectors(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![0.8, 0.8, 0.7, 0.9], // Example query vector
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    let mut wtr = Writer::from_path("search_results.csv")?;

    // Write into CSV
    wtr.write_record(&["Index", "School Name", "Location", "Score"])?;

    for (index, point) in search_result.result.iter().enumerate() {
        let school_name = point.payload.get("school_name")
            .and_then(|v| v.as_str())
            .map(|s| s.to_string()) 
            .unwrap_or("Unknown".to_string()); 
    
        let location = point.payload.get("location")
            .and_then(|v| v.as_str())
            .map(|s| s.to_string()) 
            .unwrap_or("Unknown".to_string()); 
    
        let score = point.score.to_string();
        
        wtr.write_record(&[
            &(index + 1).to_string(), 
            school_name.as_str(), 
            location.as_str(), 
            &score
        ])?;
    }    

    wtr.flush()?;

    println!("Search Results:");

    for (index, point) in search_result.result.iter().enumerate() {
        let formatted_payload = serde_json::to_string_pretty(&point.payload)
            .unwrap_or_else(|_| "Unable to format payload".to_string());

        println!("Point {}: ", index + 1);
        println!(" - Payload: {}", formatted_payload);
        println!(" - Score: {}", point.score);
        println!();
    }
    Ok(())
}

// Perform query operations on school data with filter conditions
async fn query_school_vectors_with_filter(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![0.8, 0.8, 0.7, 0.9], 
            filter: Some(Filter::all([Condition::matches(
                "location",
                "Paris".to_string(),
            )])),
            limit: 2,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    println!("Search Results with Filter:");

    for (index, point) in search_result.result.iter().enumerate() {
        let formatted_payload = serde_json::to_string_pretty(&point.payload)
            .unwrap_or_else(|_| "Unable to format payload".to_string());

        println!("Point {}: ", index + 1);
        println!(" - Payload: {}", formatted_payload);
        println!(" - Score: {}", point.score);
        println!();
    }

    Ok(())
}
