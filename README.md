# Week 7 Mini Project
> Puyang(Rivoc) Xu (px16)

This project is processing data with Vector Database. I choose Qdrant as the database in this project. The main functions include ingest data, perform queries and aggregations and visualize output.

## Detailed steps
### Preparation
The installation of `Cargo` and `Qdrant` is needed in this project.
Create a new cargo project:
```
cargo new vector_database
```

Download the latest Qdrant image from Dockerhub:
```
docker pull qdrant/qdrant
```

Then, run the service:
```
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```

Modify dependencies in `Cargo.toml`:
```
qdrant-client = "1.8.0"
tokio = { version = "1.36.0", features = ["rt-multi-thread"] }
serde_json = "1.0.114"
tonic = "0.11.0"
anyhow = "1.0.81"
csv = "1.1"
serde = { version = "1.0", features = ["derive"] }
```

### Functions
Here are some functions in `main.rs`:
* `initialize_qdrant_client` is to initializing the Qdrant Client.
* `create_school_collection` is to create a Qdrant data collection that is used to store school data. Four-dimensional vector coordinates are used here, as more generally applicable.
* `ingest_school_vectors is` to importing school data into a Qdrant collection.
* `query_school_vectors` is to perform query operations on school data.
* `query_school_vectors_with_filter` is to perform query operations on school data with filter conditions.

To visualize data more intuitively, I export the data into a csv file. Then you can use any method you like to visualize the output. I use visual.py to draw a plot.


## Results & Screenshots

### Vector Database
#### Before operations
![](./media/before.png)
#### After operations
![](./media/after.png)

### Results
![](./media/result.png)

### Visualization
#### CSV file
![](./media/csv.png)
#### Plot by python
![](./media/visual.png)